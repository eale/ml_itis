import random
import numpy as np

def crossover_function(parents, offspring_size):
    offspring = np.empty(offspring_size)
    crossover_point = np.uint8(offspring_size[1] / 2)

    for k in range(offspring_size[0]):
        parent1_idx = k % parents.shape[0]
        parent2_idx = (k + 1) % parents.shape[0]

        offspring[k, :crossover_point] = parents[parent1_idx, :crossover_point]
        offspring[k, crossover_point:] = parents[parent2_idx, crossover_point:]

    return offspring

def calculate_fitness(population, target):
    fitness = []
    for individual in population:
        fitness.append(1 / (np.abs(target - np.sum(individual * diofantov_expr)) + 1))
    return fitness

def mutate(pop_after_crossover, mutation_rate):
    population_nextgen = pop_after_crossover.copy()
    for chromosome in population_nextgen:
        for j in range(len(chromosome)):
            if random.random() < mutation_rate:
                chromosome[j] += np.random.randint(-1, 2)
    return population_nextgen

def select_parents(population, fitness, num_parents):
    parents = []
    for _ in range(num_parents):
        max_fitness_idx = np.argmax(fitness)
        parents.append(population[max_fitness_idx])
        fitness[max_fitness_idx] = -99999999
    return np.array(parents)

diofantov_expr = [7, -3, 2, -1, 10, -2]
target_value = 125

num_weights = len(diofantov_expr)
population_count = 12

population_size = (population_count, num_weights)
new_population = np.random.randint(-num_weights * 3, num_weights * 3, size=population_size)

num_generations = 1000
num_parents_mating = 6

for generation in range(num_generations):
    fitness = calculate_fitness(new_population, target_value)
    parents = select_parents(new_population, fitness, num_parents_mating)

    offspring_crossover = crossover_function(parents, (population_size[0] - parents.shape[0], num_weights))
    offspring_mutation = mutate(offspring_crossover, 0.1)

    new_population[:parents.shape[0], :] = parents
    new_population[parents.shape[0]:, :] = offspring_mutation

fitness = calculate_fitness(new_population, target_value)
best_match_idx = np.argmax(fitness)

print("Best solution : ", new_population[best_match_idx])
print("Best solution fitness : ", fitness[best_match_idx])

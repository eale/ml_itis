import pygame
from sklearn.svm import SVC

RADIUS = 5
COLORS = ['red', 'blue', 'black', 'yellow']
SCREEN_SIZE = (800, 600)
BACKGROUND_COLOR = 'white'


def main():
    pygame.init()
    screen = pygame.display.set_mode(SCREEN_SIZE)
    screen.fill(BACKGROUND_COLOR)
    pygame.display.update()

    model = SVC(kernel='linear')
    points = []
    classes = []
    learning_mode = True
    running = True

    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.MOUSEBUTTONDOWN:
                handle_mouse_button(event, learning_mode, points, classes, model, screen)
            elif event.type == pygame.KEYDOWN and event.key == pygame.K_RETURN:
                learning_mode, points, classes = toggle_learning_mode(learning_mode, points, classes, model, screen)

    pygame.quit()


def handle_mouse_button(event, learning_mode, points, classes, model, screen):
    if learning_mode:
        color_index = 0 if event.button == 1 else 1
        draw_point(event.pos, COLORS[color_index], screen)
        points.append(list(event.pos))
        classes.append(color_index)
    else:
        predict_and_draw_point(event.pos, model, screen)


def toggle_learning_mode(learning_mode, points, classes, model, screen):
    if learning_mode and len(set(classes)) > 1:
        learning_mode = False
        model.fit(points, classes)
        draw_decision_boundary(model, screen)
    elif learning_mode:
        print("Please add points of at least two different classes")
    return learning_mode, points, classes


def predict_and_draw_point(position, model, screen):
    prediction = model.predict([position])[0]
    draw_point(position, COLORS[prediction], screen)


def draw_point(position, color, screen):
    pygame.draw.circle(screen, color, position, RADIUS)
    pygame.display.update()


def draw_decision_boundary(model, screen):
    coef = model.coef_[0]
    intercept = model.intercept_[0]
    start_pos = [0, -intercept / coef[1]]
    end_pos = [SCREEN_SIZE[0], (coef[0] * SCREEN_SIZE[0] - intercept) / coef[1]]
    pygame.draw.line(screen, COLORS[2], start_pos, end_pos)
    pygame.display.update()


if __name__ == '__main__':
    main()

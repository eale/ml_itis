import copy
import numpy as np
import matplotlib.pyplot as plt
from sklearn.datasets import make_blobs


def generate_random_points(n):
    points = make_blobs(n)
    return points[0]


def calculate_distance(pointA, pointB):
    return np.sqrt((pointA[0] - pointB[0]) ** 2 + (pointA[1] - pointB[1]) ** 2)


def calculate_mean(points):
    x_mean = 0
    y_mean = 0
    count = 0

    for i in range(len(points)):
        x_mean += points[i][0]
        y_mean += points[i][1]
        count += 1

    x_mean /= count
    y_mean /= count

    return [x_mean, y_mean]


def initialize_centers(points, k):
    point_mean = calculate_mean(points)
    R = -1

    for i in range(len(points)):
        current_distance = calculate_distance(points[i], point_mean)
        if R < current_distance:
            R = current_distance
    centroids = []
    for i in range(k):
        x_c = R * np.cos(2 * np.pi * i / k) + point_mean[0]
        y_c = R * np.sin(2 * np.pi * i / k) + point_mean[1]
        centroids.append([x_c, y_c])

    return np.array(centroids)


def assign_points_to_centroids(points, centroids):
    clusters = {}

    for i in range(len(centroids)):
        clusters[i] = []

    for i in range(len(points)):
        distances = []

        for j in range(len(centroids)):
            distances.append(calculate_distance(points[i], centroids[j]))

        index = distances.index(min(distances))
        clusters[index].append(points[i])

    return clusters


def update_centroids(centroids, clusters):
    k = len(centroids)
    for i in range(k):
        if len(clusters[i]) != 0:
            centroids[i] = calculate_mean(clusters[i])
    return centroids


def are_centroids_equal(centroids, previous_centroids):
    return (centroids == previous_centroids).all()


def draw_clusters(clusters, centroids):
    for i in range(len(clusters)):
        x_coordinates = []
        y_coordinates = []
        for j in range(len(clusters[i])):
            x_coordinates.append(clusters[i][j][0])
            y_coordinates.append(clusters[i][j][1])
        plt.scatter(x_coordinates, y_coordinates)
    plt.draw()

    x_coordinates_c = []
    y_coordinates_c = []
    for i in range(len(centroids)):
        x_coordinates_c.append(centroids[i][0])
        y_coordinates_c.append(centroids[i][1])
    plt.scatter(x_coordinates_c, y_coordinates_c, color='hotpink')
    plt.draw()

    plt.show()


def kmeans_clustering(points, k, draw_iteration=False):
    centroids = initialize_centers(points, k)
    clusters = assign_points_to_centroids(points, centroids)
    previous_centroids = copy.deepcopy(centroids)

    if draw_iteration:
        draw_clusters(clusters, centroids)

    while True:
        centroids = update_centroids(centroids, clusters)
        clusters = assign_points_to_centroids(points, centroids)

        if draw_iteration:
            draw_clusters(clusters, centroids)

        if are_centroids_equal(centroids, previous_centroids):
            return {"centroids": centroids, "clusters": clusters}

        previous_centroids = copy.deepcopy(centroids)


def determine_cluster_count(inertia):
    min_inertia_ratio = 100000
    index = 0

    for i in range(len(inertia)):
        if i == 0 or i == len(inertia) - 1:
            continue
        inertia_ratio = (inertia[i] - inertia[i + 1]) / (inertia[i - 1] - inertia[i])
        if abs(inertia_ratio) < min_inertia_ratio:
            min_inertia_ratio = inertia_ratio
            index = i
    return index + 1


if __name__ == '__main__':
    n = 100

    points = generate_random_points(n)

    inertia = []
    K = range(1, 8)

    for i in K:
        km = kmeans_clustering(points, i)
        clusters = km["clusters"]
        centroids = km["centroids"]
        total_sum = 0
        for kl in range(len(clusters)):
            for c in range(len(clusters[kl])):
                total_sum += calculate_distance(centroids[kl], clusters[kl][c]) ** 2

        inertia.append(total_sum)
    plt.plot(K, inertia)
    k = determine_cluster_count(inertia)

    print(k)
    kmeans_result = kmeans_clustering(points, k, True)
